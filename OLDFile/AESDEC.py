#AES

from Crypto.Cipher import AES

key=b'TESTAESKEY'

def Pad(text):
    while len(text)%8!=0:
        text+=b' '
    return text
fs=open("OUTPUT.BIN",'rb')
nonce,tag,ciphertext=[fs.read(x)for x in (16,16,-1)]
cipher_aes = AES.new(Pad(key),AES.MODE_GCM, nonce)
data=cipher_aes.decrypt_and_verify(ciphertext, tag)
fs2=open('dec.txt','wb')
fs2.write(data)