# -*- coding: utf-8 -*-
import os
import hashlib
import json
FileInfo=[]

def file_name(file_dir): 
    for root, dirs, files in os.walk(file_dir):
        for file in files:
            print(os.path.join(root,file))
            pathsha=hashlib.sha256(os.path.join(root,file).encode('utf-8')).hexdigest()
            shares=hashlib.sha256(open(os.path.join(root,file),'rb').read()).hexdigest()
            print("Path_SHA256:"+pathsha)
            print("File_SHA256:"+shares)
            FileInfo.append({'PATH_SHA':pathsha,'FILE_SHA':shares})
    json_str=json.dumps(FileInfo)
    fs=open("Meta.json",'wb')
    fs.write(json_str.encode('utf-8'))
file_name('monitor')