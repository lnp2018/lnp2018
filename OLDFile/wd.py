#!/usr/bin/python3
# pylint: disable=C0103
# pylint: disable=C0326
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from watchdog.observers import Observer
from watchdog.events import *

class ScriptEventHandler(FileSystemEventHandler):
    def __init__(self):
        FileSystemEventHandler.__init__(self)
    def on_moved(self, event):
        what = "directory" if event.is_directory else "file"
        loggingstr="Moved %s: from %s to %s"% (what, event.src_path, event.dest_path)
        qList.addItem(loggingstr)
    def on_created(self, event):
        what = "directory" if event.is_directory else "file"
        loggingstr="Created %s: %s"%(what, event.src_path)
        qList.addItem(loggingstr)
    def on_deleted(self, event):
        what = "directory" if event.is_directory else "file"
        loggingstr="Deleted %s: %s"%( what, event.src_path)
        qList.addItem(loggingstr)
    def on_modified(self, event):
        what = "directory" if event.is_directory else "file"
        loggingstr="Modified %s: %s"%( what, event.src_path)
        qList.addItem(loggingstr)

def init(QListWdg):
    event_handler = ScriptEventHandler()
    observer = Observer()
    observer.schedule(event_handler, 'monitor', recursive=True)
    observer.start()

def on_QbutClick(self):
    qList.clear()

if __name__=='__main__':
    qApp=QApplication(sys.argv)
    w=QWidget()
    wl=QVBoxLayout()
    qList=QListWidget()
    qLab=QLabel("Monitor:")
    qbut=QPushButton("Clean")
    qbut.clicked.connect(on_QbutClick)
    wl.addWidget(qLab)
    wl.addWidget(qList)
    wl.addWidget(qbut)
    w.resize(350, 250)
    w.move(300, 300)
    w.setWindowTitle('File Change Mointor')
    w.setLayout(wl)
    w.show()
    init(qList)
    sys.exit(qApp.exec_())
