#AES

from Crypto.Cipher import AES

key=b'TESTAESKEY'

def Pad(text):
    while len(text)%8!=0:
        text+=b' '
    return text

fs=open("TEST.TXT",'rb')
newl=fs.read()
cipher_AES=AES.new(Pad(key),AES.MODE_GCM)
cipher_text,tag=cipher_AES.encrypt_and_digest(newl)
fs2=open('OUTPUT.BIN','wb')
fs2.write(cipher_AES.nonce)
fs2.write(tag)
fs2.write(cipher_text)