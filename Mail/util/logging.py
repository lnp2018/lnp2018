﻿import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from globalEnv.gEnv import global_variable

class Logging:
    def __init__(self):
        if global_variable.get_debug():
            self.log=open("log/log.txt","wb+")

    def WriteLog(self,info):
        try:
            if global_variable.get_log():
                self.log.write(''.join(info).encode('utf-8'))
                self.log.write(b'\n')
            if global_variable.get_debug():
                print(info)
        except:
            txt=sys.exc_info()
            if global_variable.get_log():
                self.log.write(''.join(info).encode('utf-8'))
                self.log.write(b'\n')
            if global_variable.get_debug():
                print(txt)
