﻿import json
#support normal dict Serialization
import sys
import os
import jsonpickle
#support for object Serialization

class json_writer:
    @staticmethod
    def write(FileName,jsonDict):
        try:
            #dic=dict((key,value)for key,value in jsonDict.items() if not key.startswith('__'))
            jsonstr=json.dumps(jsonDict)
            json_file=open(FileName,'wb')
            json_file.write(''.join(jsonstr).encode('utf-8'))
            json_file.close()
        except:
            raise

class json_reader:
    @staticmethod
    def read(FileName):
        try:
            with open(FileName) as fd:
                json_data = json.load(fd)
            return json_data
        except:
            raise

class class_json_writer:
    @staticmethod
    def write(FileName,obj):
        try:
            frozen=jsonpickle.encode(obj)
            json_file=open(FileName,'wb')
            json_file.write(''.join(frozen).encode('utf-8'))
        except:
            raise
class class_json_reader:
    @staticmethod
    def read(FileName):
        try:
            is_exist=os.path.exists(FileName)
            if not is_exist:
               raise EOFError("No JSON file Found!")
            with open(FileName) as fd:
                   return jsonpickle.decode(fd.read())
        except:
            raise