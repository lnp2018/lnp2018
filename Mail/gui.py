﻿#!/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from util.logging import Logging
from util.userdata_storage import *
from globalEnv.gEnv import global_variable
from ui.accountcfg import AccountCfg
from ui.inbox import inbox
import collections
#LOGGING SUPPORT
import sys
from protocol.protocol import *

if __name__=='__main__':
    qApp=QApplication(sys.argv)
    #for High DPI Support
    qApp.setStyle("fusion")
    qApp.setAttribute(Qt.AA_EnableHighDpiScaling)
    if hasattr(QStyleFactory, 'AA_UseHighDpiPixmaps'):
        app.setAttribute(Qt.AA_UseHighDpiPixmaps)
    #Init gEnv and Logging
    gEnv=global_variable()
    log=Logging()
    log.WriteLog("DEBUG START")
    msgans=QMessageBox.question(None,"Message","Do you want load storage account info?",QMessageBox.Yes | QMessageBox.No,QMessageBox.Yes)
    try:
        if msgans==QMessageBox.No:
            raise RuntimeError("Don't Use exist Storage!")
        log.WriteLog("LOAD STORAGE ACCOUNT INFO")
        UserInfo=json_reader.read("userdata/account.json")
    except Exception as e:
        log.WriteLog(str(e))
        log.WriteLog("CREATE NEW ACCOUNT INFO")
        UserInfo=collections.OrderedDict()
        QMessageBox.information(None,"Information","No Storage User info Find or Don't use exist info!",QMessageBox.Yes,QMessageBox.Yes)
        try:
            cfgWidget=AccountCfg()
            if cfgWidget.exec():
                SevType,RecvSevAddr,RecvSevPort,SMTP_Type,SmtpAddr,SmtpPort,uid,pwd=cfgWidget.GetAccountCfg()
                UserInfo['RecvSevType']=SevType
                UserInfo['RecvSevAddr']=RecvSevAddr
                UserInfo['RecvSevPort']=RecvSevPort
                UserInfo['SmtpType']=SMTP_Type
                UserInfo['SmtpAddr']=SmtpAddr
                UserInfo['SmtpPort']=SmtpPort
                UserInfo['Account']=uid
                UserInfo['Password']=pwd
                json_writer.write("userdata/account.json",UserInfo)
                log.WriteLog("STORAGE ACCOUNT INFO")
                QMessageBox.information(None,"Information","UserInfo Create Success!",QMessageBox.Yes,QMessageBox.Yes)
        except Exception as e:
            log.WriteLog(str(e))
            QMessageBox.critical(None,"Error",str(e))
    finally:
        try:
            log.WriteLog("ACCOUNT INFO LOADED")
            recvPrv=RecvMailProvider(UserInfo['RecvSevAddr'],int(UserInfo['RecvSevPort']),UserInfo['RecvSevType'],UserInfo['Account'],UserInfo['Password'])
            sendPrv=SendMailProvider(UserInfo['SmtpAddr'],int(UserInfo['SmtpPort']),UserInfo['SmtpType'],UserInfo['Account'],UserInfo['Password'])
            log.WriteLog("START MAIN LOOP")
            MainWdg=QMainWindow()
            MainWdg.setWindowTitle("Inbox")
            inbox_wdg=inbox(sendPrv,recvPrv,log,UserInfo)
            MainWdg.setCentralWidget(inbox_wdg)
            MainWdg.show()
        except Exception as e:
            log.WriteLog(str(e))
            QMessageBox.critical(None,"Error",str(e))
    sys.exit(qApp.exec_())