from protocol.prl_imap import imapclass
from protocol.prl_pop import popclass
from protocol.prl_smtp import smtpclass
from util.logging import Logging

class RecvMailProvider:
    def __init__(self,address,Port,ProtocolName,UserName,Password):
        try:
            if ProtocolName=="POP3_SSL":
                self.conn=popclass(address,UserName,Password,Port,True)
            elif ProtocolName=="POP3":
                self.conn=popclass(address,UserName,Password,Port,False)
            elif ProtocolName=="IMAP_SSL":
                self.conn=imapclass(address,UserName,Password,Port,True)
            elif ProtocolName=="IMAP":
                self.conn=imapclass(address,UserName,Password,Port,False)
            else:
                raise RuntimeError("Unsupported Protocol!")
        except:
            raise
    def __del__(self):
        if self.conn!=None:
            self.conn.Close()
        
    def FetchMessage(self,Top=-1):
        try:
            self.conn.GetMsgList(Top)
        except:
            raise

    def GetMessageByIndex(self,idx):
        try:
            fm,To,sub,main_body=self.conn.GetMailByIndex(idx)
            return fm,To,sub,main_body
        except:
            raise
    
    def LoadMailFromDisk(self):
        try:
            self.conn.Serialization()
        except:
            raise

    def StorageMailToDisk(self):
        try:
            self.conn.Deserialization()
        except:
            raise
    
    def GetMailListSize(self):
        try:
            return self.conn.GetListSize()
        except:
            raise




class SendMailProvider:
    def __init__(self,address,port,ProtocolName,UserName,Password):
        if ProtocolName=="SMTP_STARTTLS":
            self.smtp=smtpclass(address,UserName,Password,port,True)
        elif ProtocolName=="SMTP":
            self.smtp=smtpclass(address,UserName,Password,port,False)
        else:
            raise RuntimeError("Unsupported Protocol!")
    def __del__(self):
        if self.smtp!=None:
            self.smtp.Close()
    def SendMail(self,From,To,Sub,Body,Attach):
        try:
            self.smtp.SendMail(From,To,Sub,Body,Attach)
        except:
            raise