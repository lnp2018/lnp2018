﻿import poplib
from mailparser.mailparser import *
from util.userdata_storage import *

class popclass:
    
    def __init__(self,server,name,pw,port,ssl=True):
        try:
            if ssl:
                self.conn=poplib.POP3_SSL(server,port)
            else:
                self.conn=poplib.POP3(server,port)
            self.conn.user(name)
            self.conn.pass_(pw)
            self.msgContainer=[]
            self.mailList=[]
        except:
            raise
    @deprecated("Don't Need this!")
    def GetStat(self):
        try:
            if self.conn is None:
                raise RuntimeError("Not init or init fail!")
            else:
                ret=self.conn.stat()
                return ret
        except:
            raise
    def GetList(self):
        try:
            if self.conn is None:
                raise RuntimeError("Not init or init fail!")
            else:
                self.mailList=self.conn.list()[1];
        except:
            raise

    def GetMsgList(self,top=-1):
        if self.conn is None:
            raise RuntimeError("Not init or init fail!")
        else:
            try:
                self.GetList()
                self.msgContainer=[]
                num1=len(self.mailList)
                num0=0
                if top!=-1 and num1>top:
                    num0=num1-top
                    self.mailList=self.mailList[num1-top:num1]
                for i in range(num0+1,num1+1):
                    resp,context,octets=self.conn.retr(i)
                    msg=MailParser.GetMsg(b'\n'.join(context))
                    self.msgContainer.append(msg)
                if len(self.mailList)!=len(self.msgContainer):
                    print(len(self.mailList))
                    print(len(self.msgContainer))
                    raise RuntimeError("Internal Lists are not equal!")
            except:
                raise

    def Serialization(self):
        try:
            if len(self.msgContainer)!=0:
                class_json_writer.write("userdata/msg.json",self.msgContainer)
                class_json_writer.write("userdata/list.json",self.mailList)
        except:
            raise

    def Deserialization(self):
        try:
            self.msgContainer=class_json_reader.read("userdata/msg.json")
            self.mailList=class_json_reader.read("userdata/list.json")
            if len(self.mailList)!=len(self.msgContainer):
                    raise RuntimeError("Deserialization Fail!:File conflict!")
        except:
            raise

    def Close(self):
        try:
            if self.conn!=None:
                self.conn.quit()
                self.conn=None
        except:
            raise
    def __del__(self):
        self.Close()

    def DeleteMsg(self,idx):
        if self.conn is None:
            raise RuntimeError("Not init or init fail!")
        elif len(self.mailList)==0:
            raise RuntimeError("Get Mail First!")
        elif len(self.mailList)!=len(self.msgContainer):
            raise RuntimeError("Internal Lists are not equal!")
        else:
            try:
                pos,mailnum=self.mailList[idx]
                self.conn.dele(mailnum)
                del self.mailList[idx]
                del self.msgContainer[idx]
            except:
                raise
    def GetMailByIndex(self,idx):
        if len(self.mailList)==0:
            raise RuntimeError("Fetch Mail First!")
        elif len(self.mailList)!=len(self.msgContainer):
            raise RuntimeError("Internal Lists are not equal!")
        elif len(self.msgContainer)<idx:
            raise ValueError("Out of Index")
        else:
            try:
                msg=self.msgContainer[idx]
                Fm,To,sub,main_body=MailParser.GetMailStruct(msg)
                return Fm,To,sub,main_body
            except:
                raise
    
    def GetListSize(self):
        if len(self.mailList)==0:
            raise RuntimeError("Fetch Mail First!")
        elif len(self.mailList)!=len(self.msgContainer):
            raise RuntimeError("Internal Lists are not equal!")
        return len(self.mailList)
