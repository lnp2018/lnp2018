﻿import imaplib
from mailparser.mailparser import *
from util.userdata_storage import *


class imapclass:
    def __init__(self,server,name,pw,port,ssl=True):
        try:
            if ssl:
                self.conn=imaplib.IMAP4_SSL(server,port)
            else:
                self.conn=imaplib.IMAP4(server,port)
            self.conn.login(name,pw)
            self.conn.select("inbox")
            self.msgContainer=[]
            self.mailList=[]
        except:
            raise
    def __del__(self):
        self.Close()
    @deprecated("Don't Need this!")
    def GetStat(self):
        raise RuntimeError("IMAP Don't need this Method")
    def GetList(self):
        if self.conn is None:
               raise RuntimeError("Not init or init fail!")
        try:
            rv,data=self.conn.search(None,"ALL")
            if rv=="OK":
                for e_id in data[0].split():
                    self.mailList.append(e_id)
            else:
                raise RuntimeError("Get List Fail!")
        except:
            raise
    def GetMsgList(self,top=-1):
        if self.conn is None:
            raise RuntimeError("Not init or init fail!")
        try:
            self.GetList()
            self.msgContainer=[]
            num1=len(self.mailList)
            num0=1
            if top!=-1 and len(self.mailList)>top:
                num0=num1-top
                self.mailList=self.mailList[num0:num1]
            for e_id in self.mailList:
                rv,data=self.conn.fetch(e_id,"(RFC822)")
                if rv!="OK":
                    raise RuntimeError("Fetch Mail Fail!")
                else:
                    raw_email=data[0][1]
                    msg=MailParser.GetMsg(raw_email)
                    self.msgContainer.append(msg)
            if len(self.mailList)!=len(self.msgContainer):
                    raise RuntimeError("Internal Lists are not equal!")
        except:
            raise
    def Serialization(self):
        try:
            if len(self.msgContainer)!=0:
                class_json_writer.write("userdata/msg.json",self.msgContainer)
                class_json_writer.write("userdata/list.json",self.mailList)
        except:
            raise

    def Deserialization(self):
        try:
            self.msgContainer=class_json_reader.read("userdata/msg.json")
            self.mailList=class_json_reader.read("userdata/list.json")
            if len(self.mailList)!=len(self.msgContainer):
                    raise RuntimeError("Deserialization Fail!:File conflict!")
        except:
            raise
    def DeleteMsg(self,idx):
        if self.conn is None:
            raise RuntimeError("Not init or init fail!")
        elif len(self.mailList)==0:
            raise RuntimeError("Get Mail First!")
        elif len(self.mailList)!=len(self.msgContainer):
            raise RuntimeError("Internal Lists are not equal!")
        else:
            try:
                mailnum=self.mailList[idx]
                self.conn.store(mailnum,'+FLAGS','\\Deleted')
                del self.mailList[idx]
                del self.msgContainer[idx]
            except:
                raise
    def GetMailByIndex(self,idx):
        if len(self.mailList)==0:
            raise RuntimeError("Fetch Mail First!")
        elif len(self.mailList)!=len(self.msgContainer):
            raise RuntimeError("Internal Lists are not equal!")
        elif len(self.msgContainer)<idx:
            raise ValueError("Out of Index")
        else:
            try:
                msg=self.msgContainer[idx]
                Fm,To,sub,main_body=MailParser.GetMailStruct(msg)
                return Fm,To,sub,main_body
            except:
                raise
    def Close(self):
        try:
            if self.conn != None:
                self.conn.logout()
                self.conn=None
        except:
            raise
    
    def GetListSize(self):
        if len(self.mailList)==0:
            raise RuntimeError("Fetch Mail First!")
        elif len(self.mailList)!=len(self.msgContainer):
            raise RuntimeError("Internal Lists are not equal!")
        return len(self.mailList)