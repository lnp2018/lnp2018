﻿import smtplib
from mailparser.mailparser import *

class smtpclass:
    def __init__(self,server,name,pw,port,STARTTLS=True):
        try:
            if STARTTLS:
                self.conn=smtplib.SMTP(server,port)
                self.conn.ehlo()
                self.conn.starttls()
                self.conn.ehlo()
                self.conn.login(name, pw)
            else:
                self.conn=smtplib.SMTP(server,port)
        except:
            raise

    def SendMail(self,frm,to,subject,content,attachment):
        if self.conn==None:
            raise RuntimeError("Not init or init fail!")
        try:
            msg=MailParser.BuildMsg(frm,to,subject,content,attachment)
            self.conn.sendmail(frm,to,msg.as_string())
        except:
            raise

    def Close(self):
        try:
            if self.conn!=None:
                self.conn.quit()
                self.conn=None
        except:
            raise
    def __del__(self):
        self.Close()