﻿from util.userdata_storage import *
import collections
import os

class global_variable(object):
    def __init__(self):
        try:
            cfg_is_exist=os.path.exists("userdata/debug.json")
            if not cfg_is_exist:
                dic=collections.OrderedDict()
                dic['debug']=False
                dic['log']=False
                json_writer.write("userdata/debug.json",dic)
        except:
            raise
    #static method to ensure all module get the same value
    @staticmethod
    def get_debug():
        json_dict=json_reader.read("userdata/debug.json")
        debug=json_dict['debug']
        return debug
    @staticmethod
    def get_log():
        json_dict=json_reader.read("userdata/debug.json")
        global_variable.log=json_dict['log']
        return global_variable.log