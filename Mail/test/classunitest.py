﻿import sys
import os
from util.logging import Logging
from protocol.prl_pop import popclass
from protocol.prl_smtp import smtpclass
from protocol.prl_imap import imapclass
from protocol.protocol import *


def testStat():
    #test pop stat function
    pop=popclass('outlook.office365.com','miduoduo.zte@hotmail.com','921011zi',995,True)
    ret=pop.GetStat()
    print(ret)

def testLoginFail():
    try:
        #test error login infomation
        pop=popclass('outlook.office365.com','miduoduo.zte@hotmail.com','111',995,True)
        ret=pop.GetStat()
        print(ret)
    except Exception as e:
        print(e)

def testLoginFail2():
    try:
        #test error login infomation
        pop=popclass('outlook.office365.com','miduoduo.zte@hotmail.com','921011zi',995,True)
        pop.conn=None
        ret=pop.GetStat()
        print(ret)
    except Exception as e:
        print(e)

def testGetMsgList():
    pop=popclass('outlook.office365.com','miduoduo.zte@hotmail.com','921011zi',995,True)
    pop.GetMsgList(5);
    print(pop.msgContainer)
    print(pop.mailList)


def testserialization():
    pop=popclass('outlook.office365.com','miduoduo.zte@hotmail.com','921011zi',995,True)
    pop.GetMsgList(15);
    print(len(pop.msgContainer))
    print(len(pop.mailList))
    pop.Serialization()

def testdeserialization():
    pop=popclass('outlook.office365.com','miduoduo.zte@hotmail.com','921011zi',995,True)
    pop.Deserialization()
    print(len(pop.msgContainer))
    print(len(pop.mailList))

def testgetmsg():
    pop=popclass('outlook.office365.com','miduoduo.zte@hotmail.com','921011zi',995,True)
    #using storage message
    #pop.Deserialization()
    pop.GetMsgList(3);
    Fm,To,sub,main_body=pop.GetMailByIndex(0)
    print(Fm)
    print(To)
    print(sub)
    print(main_body)
    #with open("test.txt", 'wb') as fp:
    #    fp.write(main_body[0])

def testsendmail():
    smtp=smtpclass('smtp.office365.com','miduoduo.zte@hotmail.com','921011zi',587,True)
    smtp.SendMail('miduoduo.zte@hotmail.com','jszhtian@hotmail.com','TESTEMAIL','MAIL SEND BY PYTHON',None)
    smtp.Close()

def testgetmsgIMAP():
    pop=imapclass('outlook.office365.com','miduoduo.zte@hotmail.com','921011zi',993,True)
    #using storage message
    #pop.Deserialization()
    pop.GetMsgList(3);
    Fm,To,sub,main_body=pop.GetMailByIndex(0)
    print(Fm)
    print(To)
    print(sub)
    print(main_body)

def AbstractTest_GET_IMAP():
    recvPrv=RecvMailProvider("outlook.office365.com",993,"IMAP_SSL",'miduoduo.zte@hotmail.com','921011zi')
    recvPrv.FetchMessage(3)
    Fm,To,sub,main_body=recvPrv.GetMessageByIndex(0)
    print(Fm)
    print(To)
    print(sub)
    print(main_body)


def AbstractTest_GET_POP3():
    recvPrv=RecvMailProvider("outlook.office365.com",995,"POP3_SSL",'miduoduo.zte@hotmail.com','921011zi')
    recvPrv.FetchMessage(3)
    Fm,To,sub,main_body=recvPrv.GetMessageByIndex(0)
    print(Fm)
    print(To)
    print(sub)
    print(main_body)

def AbstractTest_SENDMAIL():
    sendPrv=SendMailProvider('smtp.office365.com',587,"SMTP_STARTTLS",'miduoduo.zte@hotmail.com','921011zi')
    sendPrv.SendMail('miduoduo.zte@hotmail.com','jszhtian@hotmail.com','TESTEMAIL20181207','MAIL SEND BY PYTHON3',None)
if __name__=='__main__':
    log=Logging()
    AbstractTest_SENDMAIL()