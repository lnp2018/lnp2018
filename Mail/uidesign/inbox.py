# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'E:\LNP\Mail\uidesign\Inbox.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_widget_Inbox(object):
    def setupUi(self, widget_Inbox):
        widget_Inbox.setObjectName("widget_Inbox")
        widget_Inbox.resize(400, 300)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(widget_Inbox)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.gridLayout.setContentsMargins(0, -1, -1, -1)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(widget_Inbox)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.FetchNum_SBox = QtWidgets.QSpinBox(widget_Inbox)
        self.FetchNum_SBox.setMinimum(0)
        self.FetchNum_SBox.setMaximum(255)
        self.FetchNum_SBox.setProperty("value", 0)
        self.FetchNum_SBox.setObjectName("FetchNum_SBox")
        self.gridLayout.addWidget(self.FetchNum_SBox, 0, 1, 1, 1)
        self.Fetch_Button = QtWidgets.QPushButton(widget_Inbox)
        self.Fetch_Button.setEnabled(True)
        self.Fetch_Button.setObjectName("Fetch_Button")
        self.gridLayout.addWidget(self.Fetch_Button, 0, 2, 1, 1)
        self.New_Button = QtWidgets.QPushButton(widget_Inbox)
        self.New_Button.setObjectName("New_Button")
        self.gridLayout.addWidget(self.New_Button, 0, 3, 1, 1)
        self.gridLayout.setColumnStretch(1, 15)
        self.verticalLayout.addLayout(self.gridLayout)
        self.tableWidget = QtWidgets.QTableWidget(widget_Inbox)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.verticalLayout.addWidget(self.tableWidget)
        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.retranslateUi(widget_Inbox)
        QtCore.QMetaObject.connectSlotsByName(widget_Inbox)

    def retranslateUi(self, widget_Inbox):
        _translate = QtCore.QCoreApplication.translate
        widget_Inbox.setWindowTitle(_translate("widget_Inbox", "Inbox"))
        self.label.setText(_translate("widget_Inbox", "Get Mails:"))
        self.Fetch_Button.setText(_translate("widget_Inbox", "Fetch"))
        self.New_Button.setText(_translate("widget_Inbox", "New"))

