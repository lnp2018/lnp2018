﻿#!/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from util.logging import Logging
from util.userdata_storage import *
from globalEnv.gEnv import global_variable
import collections
#LOGGING SUPPORT
import sys
from protocol.protocol import *

#CLI Based UI
if __name__=='__main__':
    qApp=QApplication(sys.argv)
    qApp.setStyle("fusion")
    qApp.setAttribute(Qt.AA_EnableHighDpiScaling)
    if hasattr(QStyleFactory, 'AA_UseHighDpiPixmaps'):
        app.setAttribute(Qt.AA_UseHighDpiPixmaps)
    gEnv=global_variable()
    log=Logging()
    log.WriteLog("DEBUG START")
    try:
        print("Do you want load storage account info? y/n")
        ans=input()
        if ans.lower()=="n":
            raise RuntimeError("Don't Use exist Storage!")
        log.WriteLog("LOAD STORAGE ACCOUNT INFO")
        UserInfo=json_reader.read("userdata/account.json")
    except Exception as e:
        log.WriteLog(str(e))
        log.WriteLog("CREATE NEW ACCOUNT INFO")
        UserInfo=collections.OrderedDict()
        print("No Storage User info Find or Don't use exist info!")
        print("Input Receive Server Type:")
        SevType=input()
        if SevType.upper()=="POP3_SSL":
            UserInfo['RecvSevType']="POP3_SSL"
        elif SevType.upper()=="IMAP_SSL":
            UserInfo['RecvSevType']="IMAP_SSL"
        elif SevType.upper()=="POP3":
            UserInfo['RecvSevType']="POP3"
        elif SevType.upper()=="IMAP":
            UserInfo['RecvSevType']="IMAP"
        print("Input Receive Server Address")
        RecvSevAddr=input()
        UserInfo['RecvSevAddr']=RecvSevAddr
        print("Input Receive Server Port")
        RecvSevPort=input()
        UserInfo['RecvSevPort']=RecvSevPort
        print("Input SMTP Server Type")
        SMTP_Type=input()
        if SMTP_Type.upper()=="SMTP_STARTTLS":
            UserInfo['SmtpType']="SMTP_STARTTLS"
        elif SMTP_Type.upper()=="SMTP":
            UserInfo['SmtpType']="SMTP"
        print("Input SMTP Server Address")
        SmtpAddr=input()
        UserInfo['SmtpAddr']=SmtpAddr
        print("Input SMTP Server Port")
        SmtpPort=input()
        UserInfo['SmtpPort']=SmtpPort
        print("Input UserName")
        uid=input()
        UserInfo['Account']=uid
        print("Input Password")
        pwd=input()
        UserInfo['Password']=pwd
        json_writer.write("userdata/account.json",UserInfo)
        log.WriteLog("STORAGE ACCOUNT INFO")
        print("UserInfo Create Success!")
    finally:
        log.WriteLog("ACCOUNT INFO LOADED")
        recvPrv=RecvMailProvider(UserInfo['RecvSevAddr'],int(UserInfo['RecvSevPort']),UserInfo['RecvSevType'],UserInfo['Account'],UserInfo['Password'])
        sendPrv=SendMailProvider(UserInfo['SmtpAddr'],int(UserInfo['SmtpPort']),UserInfo['SmtpType'],UserInfo['Account'],UserInfo['Password'])
        log.WriteLog("START MAIN LOOP")
        while(True):
            cmd=input("\nInput command(use help to get all command lists):")
            if cmd.lower()=="help":
                log.WriteLog("SELECT HELP METHOD")
                print("help|get all commands")
                print("fetch x|fetch x mails from server")
                print("get x|get num x mail")
                print("list|get the maillist which has been fetched")
                print("send|send message")
                print("quit|exit application")
            elif cmd.lower()=="quit":
                log.WriteLog("SELECT QUIT METHOD")
                print("If the logging function is enable, please close the logging window")
                break
            elif cmd.lower()=="send":
                log.WriteLog("SELECT SEND METHOD")
                try:
                    Frm=input("Send From("+UserInfo['Account']+"):")
                    To=input("Send To:")
                    Sub=input("Subject:")
                    Main_body=input("Content:")
                    if len(Frm)==0:
                        Frm=UserInfo['Account']
                    if len(To)==0:
                        raise RuntimeError("No Receiver's address!")
                    sendPrv.SendMail(Frm,To,Sub,Main_body,None)
                    print("Operation Finish!")
                except Exception as e:
                    print("ERROR:"+str(e))
                    log.WriteLog("ERROR:"+str(e))
            elif cmd.lower().startswith("fetch"):
                log.WriteLog("SELECT FETCH METHOD")
                try:
                    if len(cmd.lower())==5:
                        recvPrv.FetchMessage()
                    else:
                        Num_Top=int(cmd.lower()[6:])
                        recvPrv.FetchMessage(Num_Top)
                    print("Operation Finish!")
                except Exception as e:
                    print("ERROR:"+str(e))
                    log.WriteLog("ERROR:"+str(e))
            elif cmd.lower().startswith("get"):
                log.WriteLog("SELECT GET METHOD")
                try:
                    Num_Idx=int(cmd.lower()[3:])
                    Fm,To,sub,main_body=recvPrv.GetMessageByIndex(Num_Idx)
                    print(Fm)
                    print(To)
                    print(sub)
                    print(main_body)
                    print("Operation Finish!")
                except Exception as e:
                    print("ERROR:"+str(e))
                    log.WriteLog("ERROR:"+str(e))
            elif cmd.lower()=="list":
                log.WriteLog("SELECT LIST METHOD")
                try:
                    listlen=recvPrv.GetMailListSize()
                    print("INDEX|FROM|TO|SUBJECT")
                    for i in range(listlen):
                        Fm,To,sub,main_body=recvPrv.GetMessageByIndex(i)
                        print(str(i)+"|"+Fm+"|"+To+"|"+sub)
                except Exception as e:
                    print("ERROR:"+str(e))
                    log.WriteLog("ERROR:"+str(e))
            else:
                log.WriteLog("SELECT UNEXPECTED METHOD")
                print("Undefined Operation! Please use help method!")

    sys.exit(qApp.exec_())