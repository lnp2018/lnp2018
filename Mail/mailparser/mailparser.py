﻿import email
import types
import chardet
from os.path import basename
from email.parser import Parser
from email.header import decode_header
from email.utils import parseaddr,formataddr,COMMASPACE, formatdate
from email import encoders
from email.header import Header
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from util.deprecated import deprecated


class MailParser(object):

    @staticmethod
    #@deprecated("Use method GetMailStruct instead")
    def GetMsg(mailBytes):
        try:
            msg=email.message_from_bytes(mailBytes)
            return msg
        except:
            return None
            raise
    @staticmethod
    def GetMailStruct(msg_obj):
        var_from=msg_obj.get_all("From")
        val_from=decode_header(var_from[0])
        FrmStr=""
        #va_from=val_from[0][0]
        for var in val_from:  
            if type(var) is not str and var[1] is not None:
                var_frm=var[0].decode(var[1])
            elif var is None:
                continue
            else:
                if type(var[0]) is not str:
                    encoding=chardet.detect(var[0])['encoding']
                    var_frm=var[0].decode(encoding)
                else:
                    var_frm=var[0]
            FrmStr+=var_frm+","
        var_to=msg_obj.get_all("To")
        val_to=decode_header(var_to[0])
        ToStr=""
        for var in val_to:
            if type(var) is not str and var[1] is not None:
                va_to=var[0].decode(var[1])
            elif var is None:
                continue
            else:
                if type(var[0]) is not str:
                    encoding=chardet.detect(var[0])['encoding']
                    va_to=var[0].decode(encoding)
                else:
                    va_to=var[0]
            ToStr+=va_to+","
        var_sub=msg_obj.get_all("Subject")
        val_sub=decode_header(var_sub[0])
        SubStr=""
        for var in val_sub:
            if type(var) is not str and var[1] is not None:
                va_sub=var[0].decode(var[1])
            elif var is None:
                continue
            else:
                if type(var[0]) is not str:
                    encoding=chardet.detect(var[0])['encoding']
                    va_sub=var[0].decode(encoding)
                else:
                    va_sub=var[0]
            SubStr+=va_sub+","
        main_body=[]
        for part in msg_obj.walk():
            if part.get_content_maintype() == 'multipart':
                continue
            if part.get_content_type()=='text/plain'or part.get_content_type()=='text/html':
                content=part.get_payload(decode=True)
                #detect charset by attribute charset
                encoding=part.get_charset()
                if encoding==None:
                    #detect charset by parsing head
                    content_type = part.get('Content-Type', '').lower()
                    pos = content_type.find('charset=')
                    if pos>=0:
                        encoding=content_type[pos + 8:].strip()
                if encoding==None:
                    #guess charset using chardet
                    encoding=chardet.detect(content)['encoding']
                content=content.decode(encoding)
                main_body.append(content)
        return FrmStr[0:-1],ToStr[0:-1],SubStr[0:-1],main_body
    @staticmethod
    def BuildMsg(frm,to,subject,content,attachment):
        msg = MIMEMultipart()
        msg['From']=frm
        msg['To']=to
        msg['Subject']=subject
        msg.attach(MIMEText(content,'plain','utf-8'))
        if attachment!=None:
            with open(attachment,'rb') as fd:
                att_part=MIMEApplication(fd.read(),Name=basename(fd))
                part['Content-Disposition']= 'attachment; filename="%s"' % basename(fd)
                msg.attach(part)
        return msg