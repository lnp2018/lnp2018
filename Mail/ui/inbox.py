﻿# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'E:\LNP\Mail\uidesign\Inbox.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from ui.MailEditorWidgets import MailEditor
from PyQt5.QtWidgets import *

class Ui_widget_Inbox(object):
    def setupUi(self, widget_Inbox):
        widget_Inbox.setObjectName("widget_Inbox")
        widget_Inbox.resize(400, 300)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(widget_Inbox)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.gridLayout.setContentsMargins(0, -1, -1, -1)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(widget_Inbox)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.FetchNum_SBox = QtWidgets.QSpinBox(widget_Inbox)
        self.FetchNum_SBox.setMinimum(0)
        self.FetchNum_SBox.setMaximum(255)
        self.FetchNum_SBox.setProperty("value", 0)
        self.FetchNum_SBox.setObjectName("FetchNum_SBox")
        self.gridLayout.addWidget(self.FetchNum_SBox, 0, 1, 1, 1)
        self.Fetch_Button = QtWidgets.QPushButton(widget_Inbox)
        self.Fetch_Button.setEnabled(True)
        self.Fetch_Button.setObjectName("Fetch_Button")
        self.gridLayout.addWidget(self.Fetch_Button, 0, 2, 1, 1)
        self.New_Button = QtWidgets.QPushButton(widget_Inbox)
        self.New_Button.setObjectName("New_Button")
        self.gridLayout.addWidget(self.New_Button, 0, 3, 1, 1)
        self.gridLayout.setColumnStretch(1, 15)
        self.verticalLayout.addLayout(self.gridLayout)
        self.tableWidget = QtWidgets.QTableWidget(widget_Inbox)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.verticalLayout.addWidget(self.tableWidget)
        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.retranslateUi(widget_Inbox)
        self.tableWidget.setColumnCount(3)
        self.tableWidget.setHorizontalHeaderLabels(['FROM','TO','SUBJECT'])
        self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.tableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tableWidget.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.tableWidget.setSelectionMode(QAbstractItemView.SingleSelection)
        QtCore.QMetaObject.connectSlotsByName(widget_Inbox)

    def retranslateUi(self, widget_Inbox):
        _translate = QtCore.QCoreApplication.translate
        widget_Inbox.setWindowTitle(_translate("widget_Inbox", "Inbox"))
        self.label.setText(_translate("widget_Inbox", "Get Mails:"))
        self.Fetch_Button.setText(_translate("widget_Inbox", "Fetch"))
        self.New_Button.setText(_translate("widget_Inbox", "New"))


class inbox(QWidget):
    def __init__(self,SEND_HANDLE,RECV_HANDLE,LOG_OBJ,UserInfo):
        self.SEND=SEND_HANDLE
        self.RECV=RECV_HANDLE
        self.LOG=LOG_OBJ
        self.USERINFO=UserInfo
        super(inbox,self).__init__()
        self.ui=Ui_widget_Inbox()
        self.ui.setupUi(self)
        self.ui.New_Button.clicked.connect(self.onClickNew)
        self.ui.Fetch_Button.clicked.connect(self.onClickFetch)
        self.ui.tableWidget.doubleClicked.connect(self.onDoubleClickTable)
    def onClickNew(self):
        self.LOG.WriteLog("SELECT SEND METHOD")
        try:
            MailWdg=MailEditor(self.USERINFO['Account'])
            ret=MailWdg.exec()
            if(ret==MailEditor.Accepted):
                Frm,To,Sub,Main_body=MailWdg.GetMailContent()
                if len(Frm)==0:
                    Frm=self.USERINFO['Account']
                if len(To)==0:
                    raise RuntimeError("No Receiver's address!")
                self.SEND.SendMail(Frm,To,Sub,Main_body,None)
                self.LOG.WriteLog("Operation Finish!")
                QMessageBox.information(None,"Information","Operation Finish!",QMessageBox.Yes,QMessageBox.Yes)
            else:
                raise RuntimeError("Send Cancelled")
        except Exception as e:
            self.LOG.WriteLog("ERROR:"+str(e))
            QMessageBox.critical(None,"Error",str(e))

    def onDoubleClickTable(self):
        try:
            row_idx=self.ui.tableWidget.currentRow()
            self.LOG.WriteLog("SELECT ROW:"+str(row_idx))
            Fm,To,sub,main_body=self.RECV.GetMessageByIndex(row_idx)
            MainContent=""
            for var in main_body:
               MainContent+=var+"\n"
            MailWdg=MailEditor(Fm,To,sub,MainContent)
            MailWdg.exec()
        except Exception as e:
           self.LOG.WriteLog("ERROR:"+str(e))
           QMessageBox.critical(None,"Error",str(e))

    def onClickFetch(self):
        self.LOG.WriteLog("SELECT FETCH METHOD")
        try:
            self.ui.tableWidget.setRowCount(0)
            self.ui.tableWidget.setHorizontalHeaderLabels(['FROM','TO','SUBJECT'])
            Num_Top=self.ui.FetchNum_SBox.value()
            self.LOG.WriteLog("Start Fetch mails!")
            if Num_Top!=0:
                self.RECV.FetchMessage(Num_Top)
            else:
                self.RECV.FetchMessage()
            listlen=self.RECV.GetMailListSize()
            self.ui.tableWidget.setRowCount(listlen)
            for i in range(listlen):
                Fm,To,Sub,Main_body=self.RECV.GetMessageByIndex(i)
                self.ui.tableWidget.setItem(i,0,QtWidgets.QTableWidgetItem(Fm))
                self.ui.tableWidget.setItem(i,1,QtWidgets.QTableWidgetItem(To))
                self.ui.tableWidget.setItem(i,2,QtWidgets.QTableWidgetItem(Sub))
            self.LOG.WriteLog("Operation Finish!")
        except Exception as e:
            self.LOG.WriteLog("ERROR:"+str(e))
            QMessageBox.critical(None,"Error",str(e))