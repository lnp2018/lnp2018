﻿# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'E:\LNP\Mail\uidesign\MailEditor.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QDialog

class Ui_MailEditor(object):
    def setupUi(self, MailEditor):
        MailEditor.setObjectName("MailEditor")
        MailEditor.resize(997, 514)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(MailEditor)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.lineEdit_2 = QtWidgets.QLineEdit(MailEditor)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.gridLayout_2.addWidget(self.lineEdit_2, 1, 1, 1, 1)
        self.lineEdit_3 = QtWidgets.QLineEdit(MailEditor)
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.gridLayout_2.addWidget(self.lineEdit_3, 4, 1, 1, 1)
        self.label_4 = QtWidgets.QLabel(MailEditor)
        self.label_4.setObjectName("label_4")
        self.gridLayout_2.addWidget(self.label_4, 5, 0, 1, 1)
        self.lineEdit = QtWidgets.QLineEdit(MailEditor)
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout_2.addWidget(self.lineEdit, 0, 1, 1, 1)
        self.label_3 = QtWidgets.QLabel(MailEditor)
        self.label_3.setObjectName("label_3")
        self.gridLayout_2.addWidget(self.label_3, 4, 0, 1, 1)
        self.label = QtWidgets.QLabel(MailEditor)
        self.label.setObjectName("label")
        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(MailEditor)
        self.label_2.setObjectName("label_2")
        self.gridLayout_2.addWidget(self.label_2, 1, 0, 1, 1)
        self.Send_Button = QtWidgets.QPushButton(MailEditor)
        self.Send_Button.setObjectName("Send_Button")
        self.gridLayout_2.addWidget(self.Send_Button, 0, 2, 1, 1)
        self.Clean_Button = QtWidgets.QPushButton(MailEditor)
        self.Clean_Button.setObjectName("Clean_Button")
        self.gridLayout_2.addWidget(self.Clean_Button, 1, 2, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout_2)
        self.textEdit = QtWidgets.QTextEdit(MailEditor)
        self.textEdit.setObjectName("textEdit")
        self.verticalLayout.addWidget(self.textEdit)
        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.retranslateUi(MailEditor)
        self.Send_Button.clicked.connect(MailEditor.accept)
        QtCore.QMetaObject.connectSlotsByName(MailEditor)

    def retranslateUi(self, MailEditor):
        _translate = QtCore.QCoreApplication.translate
        MailEditor.setWindowTitle(_translate("MailEditor", "MailEditor"))
        self.label_4.setText(_translate("MailEditor", "Content:"))
        self.label_3.setText(_translate("MailEditor", "Subject:"))
        self.label.setText(_translate("MailEditor", "From:"))
        self.label_2.setText(_translate("MailEditor", "To:"))
        self.Send_Button.setText(_translate("MailEditor", "Send"))
        self.Clean_Button.setText(_translate("MailEditor", "Clean"))

    


class MailEditor(QDialog):
    def __init__(self,Frm='',To='',Sub='',Cont=''):
        super(MailEditor,self).__init__()
        self.ui=Ui_MailEditor()
        self.ui.setupUi(self)
        self.ui.lineEdit.setText(Frm)
        self.ui.lineEdit_2.setText(To)
        self.ui.lineEdit_3.setText(Sub)
        self.ui.textEdit.setText(Cont)
        self.ui.Clean_Button.clicked.connect(self.onCleanClicked)

    def GetMailContent(self):
        return self.ui.lineEdit.text(),self.ui.lineEdit_2.text(),self.ui.lineEdit_3.text(),self.ui.textEdit.toPlainText()

    def onCleanClicked(self):
        self.ui.lineEdit.setText("")
        self.ui.lineEdit_2.setText("")
        self.ui.lineEdit_3.setText("")
        self.ui.textEdit.setText("")

